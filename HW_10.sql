-- Database: hw_10

-- DROP DATABASE hw_10;

CREATE DATABASE hw_10
    WITH 
    OWNER = postgres
    ENCODING = 'UTF8'
    LC_COLLATE = 'Russian_Ukraine.1251'
    LC_CTYPE = 'Russian_Ukraine.1251'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;

-- DROP TABLE address CASCADE;
-- DROP TABLE public.user CASCADE;
-- DROP TABLE location;
-- DROP TABLE user_address_relation;

CREATE TABLE IF NOT EXISTS address(
	id SERIAL PRIMARY KEY,
	street VARCHAR(55) NOT NULL,
	city VARCHAR(55) NOT NULL,
	country VARCHAR(55) NOT NULL,
	zip_code INT
);

CREATE TABLE IF NOT EXISTS location(
	id SERIAL PRIMARY KEY,
	name VARCHAR(55) NOT NULL,
	score INT UNIQUE NOT NULL,
	address_id INT NOT NULL,
	FOREIGN KEY (address_id) REFERENCES address(id) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS public.user(
	id SERIAL PRIMARY KEY,
	first_name VARCHAR(55) NOT NULL,
	last_name VARCHAR(55),
	age INT
);

CREATE TABLE IF NOT EXISTS user_address_relation(
	user_id INT NOT NULL,
	address_id INT NOT NULL,
	FOREIGN KEY (user_id) REFERENCES public.user(id) ON DELETE CASCADE,
	FOREIGN KEY (address_id) REFERENCES address(id) ON DELETE CASCADE,
	UNIQUE(user_id, address_id)
);

INSERT INTO address(street, city, country, zip_code)
	VALUES('First street', 'Zaporigga', 'Ukraine', 69000),
		('Second street', 'Lviv', 'Ukraine', 78210),
		('Third street', 'Kiev', 'Ukraine', 10000),
		('Four street', 'Lviv', 'Ukraine', 78000),
		('Five street', 'Kiev', 'Ukraine', 11000),
		('Six street', 'Zaporigga', 'Ukraine', 69060);
		 
-- SELECT * FROM address;

INSERT INTO location(name, score, address_id)
	VALUES('Location 1', 87, 6),
		('Location 2', 43, 5),
		('Location 3', 21, 4),
		('Location 4', 25, 3),
		('Location 5', 73, 2),
		('Location 6', 29, 1),
		('Location 7', 12, 2),
		('Location 8', 92, 3),
		('Location 9', 81, 4),
		('Location 10', 80, 5),
		('Location 11', 05, 6),
		('Location 12', 99, 5),
		('Location 13', 91, 4),
		('Location 14', 24, 3),
		('Location 15', 74, 2);
		
-- SELECT * FROM location;

INSERT INTO public.user(first_name, last_name, age)
	VALUES('John', 'Doe', 100),
		('Eric', NULL, NULL),
		('Margarret', NULL, 21),
		('Johna', 'Hex', NULL),
		('Max', 'Fun', 10),
		('John', 'Week', 40),
		('Brandon', 'Frazer', 45),
		('Marcus', NULL, 18),
		('Natali', 'Orero', 40),
		('Alla', 'Puga4eva', 90);
		
-- SELECT * FROM public.user;

INSERT INTO user_address_relation(user_id, address_id)
	VALUES(1,1),
		(1,3),
		(1,6),
		(2,2),
		(2,5),
		(3,4),
		(4,6),
		(5,1),
		(6,2),
		(6,3),
		(6,4),
		(7,1),
		(8,1),
		(9,2),
		(10,6),
		(10,5);
		
-- SELECT * FROM user_address_relation;

SELECT public.user.first_name, public.user.last_name, address.city, address.street, address.zip_code
	FROM public.user JOIN user_address_relation ON public.user.id = user_address_relation.user_id
	JOIN address ON user_address_relation.address_id = address.id
	
SELECT public.user.first_name, public.user.last_name, public.user.age,
	address.country, address.city, address.street, address.zip_code
	FROM public.user JOIN user_address_relation ON public.user.id = user_address_relation.user_id
	JOIN address ON user_address_relation.address_id = address.id
	WHERE address.street = 'First street'

SELECT location.name, address.zip_code, address.city FROM location JOIN address 
	ON location.address_id = address.id WHERE address.city = 'Zaporigga'
